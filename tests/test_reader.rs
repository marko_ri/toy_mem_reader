use std::io::{self, BufRead, BufReader};
use std::path::PathBuf;
use std::process::{Command, Stdio};
use std::sync::Once;

use toy_mem_reader::*;

static INIT: Once = Once::new();

// First compile the test_program
pub fn test_init() {
    INIT.call_once(|| {
        if !path_test_program().unwrap().exists() {
            println!("TEST INIT: compiling test_program");
            Command::new("cargo")
                .args(["build", "--example", "test_program"])
                .spawn()
                .unwrap();
        }
    });
}

#[test]
fn test_read_small() {
    test_init();
    let mem = read_test_program(None).unwrap();
    assert_eq!(mem, (0..32_u8).collect::<Vec<u8>>());
}

#[test]
fn test_read_large() {
    test_init();
    // 20,000 should be greater than a single page on most systems.
    // macOS on ARM is 16384.
    const SIZE: usize = 20_000;
    let arg = format!("{}", SIZE);
    let mem = read_test_program(Some(&[&arg])).unwrap();
    let expected = (0..SIZE)
        .map(|x| (x % (u8::MAX as usize + 1)) as u8)
        .collect::<Vec<u8>>();
    assert_eq!(mem, expected);
}

fn path_test_program() -> Option<PathBuf> {
    let result = std::env::current_exe()
        .ok()?
        .parent()?
        .parent()?
        .join("examples/test_program");
    Some(result)
}

fn read_test_program(args: Option<&[&str]>) -> io::Result<Vec<u8>> {
    // spawn a child process and attempt to read its memory
    let path = path_test_program().unwrap();
    let mut cmd = Command::new(&path);
    {
        cmd.stdin(Stdio::piped()).stdout(Stdio::piped());
    }
    if let Some(a) = args {
        cmd.args(a);
    }

    let mut child = cmd.spawn()?;
    let handle = ProcessHandle::try_from(&child)?;

    // "examples/test_program.rs" prints the address and size
    let reader = BufReader::new(child.stdout.take().unwrap());
    let line = reader.lines().next().unwrap().unwrap();
    let bits = line.split_ascii_whitespace().collect::<Vec<_>>();
    let addr = usize::from_str_radix(&bits[0][2..], 16).unwrap();
    let size = bits[1].parse::<usize>().unwrap();

    let mem = copy_address(addr, size, &handle)?;
    child.wait()?;

    Ok(mem)
}
