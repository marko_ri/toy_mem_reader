// 1. read string from standard input and store it on the stack
// 2. read string from standard input and sotore it on the heap
// 3. sleep for long time
// => now we can inspect the memory space of this process

use std::io::{self, Read, Write};
use std::{thread, time};

const STR_LEN: usize = 64;

fn main() -> io::Result<()> {
    let mut stack_str = [0_u8; STR_LEN];
    let mut heap_str = String::with_capacity(STR_LEN);

    input(&mut stack_str, &mut heap_str)?;

    // clear screen
    println!("\x1b[2J\x1b[0;0H");

    // simulate crash
    println!("Program has become unresponsive");
    thread::sleep(time::Duration::from_secs(60 * 60));

    Ok(())
}

fn input(stack_str: &mut [u8], heap_str: &mut String) -> io::Result<()> {
    // get strings
    print!("[Stack String] >> ");
    io::stdout().flush()?;
    let _n = io::stdin().read(stack_str)?;

    print!("[Heap String] >> ");
    io::stdout().flush()?;
    io::stdin().read_line(heap_str)?;

    Ok(())
}

/*
fn test(stack_str: &mut [u8], heap_str: &mut String) {
    for (i, c) in "HelloStack".bytes().enumerate() {
        stack_str[i] = c;
    }
    for (i, c) in "HelloHeap".chars().enumerate() {
        heap_str.insert(i, c);
    }
}
*/
