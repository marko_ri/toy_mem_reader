use std::env;
use std::io::{self, BufRead, BufReader, Read, Result};
use std::process::{Command, Stdio};
use toy_mem_reader::*;

fn main() -> Result<()> {
    if env::args_os().len() > 1 {
        // we are the child
        return in_child();
    }

    // run this executable again so we have a child process to read.
    let mut child = Command::new(env::current_exe()?)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .arg("child")
        .spawn()?;

    // get a ProcessHandle to work with
    let handle = ProcessHandle::try_from(&child)?;

    // the child process will print the address to read from on stdout
    let mut stdout = BufReader::new(child.stdout.take().unwrap());
    let mut addr_string = String::new();
    stdout.read_line(&mut addr_string)?;
    let address = usize::from_str_radix(addr_string.trim(), 16).unwrap();

    // try to read 10 bytes from that address
    let bytes = copy_address(address, 10, &handle)?;
    println!("Read: {:?}", bytes);

    // tell the child to exit by closing its stdin
    drop(child.stdin.take());
    // and wait for it to exit
    child.wait()?;

    Ok(())
}

fn in_child() -> Result<()> {
    // allocate a 10-byte Vec for the parent to read
    let readable_bytes = vec![1_u8, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // print the address of the Vec to stdout so the parent can find it
    println!("{:x}", readable_bytes.as_ptr() as usize);

    // now wait to exit until the parent closes our stdin, to give it time
    // to read the memory
    let mut buf = Vec::new();
    // we do not care if this succeeds
    drop(io::stdin().read_to_end(&mut buf));
    Ok(())
}
