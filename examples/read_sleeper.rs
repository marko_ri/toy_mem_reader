use std::{env, fs, io::Write};
use toy_mem_reader::*;

/*
  1. start external process: cargo run --example sleeper
     (remember string_you_inserted)
  2. cargo run --example read_sleeper -- `pidof sleeper`
     (will create new file 'dump.bin')
  3. strings dump.bin | grep string_you_inserted
     (those strings should be somewhere in the memory dump)
*/
fn main() {
    let pid = env::args().nth(1).unwrap().parse::<i32>().unwrap();
    let handle = ProcessHandle::try_from(pid).unwrap();
    let mut file = fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open("dump.bin")
        .unwrap();
    let memory_ranges = read_maps_info(pid);
    for memory in memory_ranges.iter() {
        let bytes = copy_address(memory.start, memory.range_size(), &handle).unwrap();
        file.write_all(&bytes).unwrap();
    }
}

/*
fn bytes_to_hex(bytes: &[u8]) -> String {
    let hex_bytes: Vec<String> = bytes.iter().map(|b| format!("{:02x}", b)).collect();
    hex_bytes.join("")
}
*/

#[derive(Debug)]
struct Memory {
    start: usize,
    end: usize,
}

impl Memory {
    fn new(start: &str, end: &str) -> Self {
        Self {
            start: usize::from_str_radix(start, 16).unwrap(),
            end: usize::from_str_radix(end, 16).unwrap(),
        }
    }

    fn range_size(&self) -> usize {
        self.end - self.start
    }
}

fn read_maps_info(pid: i32) -> Vec<Memory> {
    let path = format!("/proc/{}/maps", pid);
    let file = fs::read_to_string(path).unwrap();

    let mut result = Vec::new();
    for line in file.lines() {
        let mut iter_mem = line.trim().split_ascii_whitespace();
        let mem_range = iter_mem.next().unwrap();
        let mem_desc = iter_mem.last().unwrap();

        if mem_desc == "[stack]" || mem_desc == "[heap]" {
            let mut iter_addr = mem_range.split('-');
            let addr_start = iter_addr.next().unwrap();
            let addr_end = iter_addr.next().unwrap();

            // println!("{mem_desc}-start: {addr_start}");
            // println!("{mem_desc}-end: {addr_end}");

            let mem = Memory::new(addr_start, addr_end);
            result.push(mem);
        }
    }
    result
}
