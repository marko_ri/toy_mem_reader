// Read bytes from the current process.

use toy_mem_reader::*;

fn main() {
    let data = vec![0_u8, 1, 2, 3];
    let data_addr = data.as_ptr() as usize;
    let self_pid = unsafe { libc::getpid() };
    let self_handle = ProcessHandle::try_from(self_pid).unwrap();
    let bytes = copy_address(data_addr, data.len(), &self_handle).unwrap();
    assert_eq!(bytes, data);
}
