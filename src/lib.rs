// Read memory from another process address space. Linux only.
// (you are most likely to succeed if you are attempting to read a process
// that you have spawned yourself)

use std::{
    fs,
    io::{self, Read, Seek},
    process::Child,
};

pub struct ProcessHandle(libc::pid_t);

impl TryFrom<libc::pid_t> for ProcessHandle {
    type Error = io::Error;

    fn try_from(pid: libc::pid_t) -> io::Result<Self> {
        Ok(Self(pid))
    }
}

impl TryFrom<&Child> for ProcessHandle {
    type Error = io::Error;

    fn try_from(child: &Child) -> io::Result<Self> {
        Self::try_from(child.id() as libc::pid_t)
    }
}

impl ProcessHandle {
    pub fn copy_address(&self, addr: usize, buf: &mut [u8]) -> io::Result<()> {
        let local_iov = libc::iovec {
            iov_base: buf.as_mut_ptr() as *mut libc::c_void,
            iov_len: buf.len(),
        };
        let remote_iov = libc::iovec {
            iov_base: addr as *mut libc::c_void,
            iov_len: buf.len(),
        };

        let transfer_result =
            unsafe { libc::process_vm_readv(self.0, &local_iov, 1, &remote_iov, 1, 0) };

        if transfer_result == -1 {
            match io::Error::last_os_error().raw_os_error() {
                Some(libc::ENOSYS) | Some(libc::EPERM) => self.read_proc_mem(addr, buf),
                _ => Err(io::Error::last_os_error()),
            }
        } else {
            Ok(())
        }
    }

    // Fallback method if 'process_vm_readv' fails
    fn read_proc_mem(&self, addr: usize, buf: &mut [u8]) -> io::Result<()> {
        let mut proc_mem = fs::File::open(format!("/proc/{}/mem", self.0))?;
        proc_mem.seek(io::SeekFrom::Start(addr as u64))?;
        proc_mem.read_exact(buf)
    }
}

/// Copy `length` bytes of memory at `addr` from `source`
pub fn copy_address(addr: usize, length: usize, source: &ProcessHandle) -> io::Result<Vec<u8>> {
    let mut buf = vec![0; length];
    source.copy_address(addr, &mut buf).and(Ok(buf))
}
