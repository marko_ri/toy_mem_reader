
Read memory from another process (you may not always have permission to do so).

Inspired by this video: [Getting up in another processes memory](https://www.youtube.com/watch?v=0ihChIaN8d0)
and this project: [read-process-memory](https://github.com/rbspy/read-process-memory)

This is just a toy example that works on Linux. For the complete example look at the linked project.
